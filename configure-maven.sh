#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>Artifactory</id>\
  <username>${AF_USER}</username>\
  <password>${AF_PASSWORD}</password>\
</server>" /usr/share/maven/conf/settings.xml

sed -i "/<profiles>/ a\
<profile>\
  <id>Artifactory</id>\
  <activation>\
    <activeByDefault>true</activeByDefault>\
  </activation>\
  <repositories>\
    <repository>\
      <id>Artifactory</id>\
      <url>https://biotelemetry.jfrog.io/biotelemetry/msa2</url>\
    </repository>\
  </repositories>\
  <pluginRepositories>\
    <pluginRepository>\
      <id>Artifactory</id>\
      <url>https://biotelemetry.jfrog.io/biotelemetry/msa2</url>\
    </pluginRepository>\
  </pluginRepositories>\
</profile>" /usr/share/maven/conf/settings.xml