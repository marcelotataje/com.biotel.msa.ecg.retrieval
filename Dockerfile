FROM openjdk:11.0-jre as runtime
COPY target/EcgRetrieval.jar app.jar
EXPOSE 8083 8183
ENTRYPOINT ["sh", "-c", "java -jar -Dspring.profiles.active=$ENV_PROFILE /app.jar"]