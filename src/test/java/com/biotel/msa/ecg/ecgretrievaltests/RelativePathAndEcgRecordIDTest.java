package com.biotel.msa.ecg.ecgretrievaltests;

import com.biotel.msa.ecg.retrieval.model.data.RelativePathAndEcgRecordID;

public class RelativePathAndEcgRecordIDTest implements RelativePathAndEcgRecordID {
    
    public String relativePath = "\\test\\";
    public long ecgRecordID = 132;
    public String getRelativePath() { return relativePath;}
    public long getEcgRecordID() { return ecgRecordID;}
}
