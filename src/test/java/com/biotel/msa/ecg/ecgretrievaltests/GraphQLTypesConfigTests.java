package com.biotel.msa.ecg.ecgretrievaltests;

import com.biotel.msa.ecg.retrieval.api.graphql.types.GraphQLByteArray;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {GraphQLByteArray.class})
public class GraphQLTypesConfigTests {

    @Autowired
    private GraphQLByteArray graphQLByteArray;

    @Autowired
    private ApplicationContext context;

    @Test
    public void graphQLByteArrayScalar_exists() {
        Assert.assertNotNull("GraphQLByteArray bean not injected.", graphQLByteArray);
    }

}
