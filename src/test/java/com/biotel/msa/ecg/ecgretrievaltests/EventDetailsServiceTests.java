package com.biotel.msa.ecg.ecgretrievaltests;

import com.biotel.msa.ecg.retrieval.config.CustomECGDataException;
import com.biotel.msa.ecg.retrieval.model.dto.EventDetails;
import com.biotel.msa.ecg.retrieval.model.dto.EventQueue;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.*;
import com.biotel.msa.ecg.retrieval.persistance.enums.EntityType;
import com.biotel.msa.ecg.retrieval.persistance.enums.EventStatus;
import com.biotel.msa.ecg.retrieval.persistance.enums.Preference;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.*;
import com.biotel.msa.ecg.retrieval.service.EcgStripService;
import com.biotel.msa.ecg.retrieval.service.EventDetailsService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Configuration;

import java.time.Instant;
import java.util.*;

@RunWith(JUnit4.class)
public class EventDetailsServiceTests {

    @InjectMocks
    EventDetailsService eventDetailsService;

    @Mock
    private ECGStripRepository ecgStripRepositoryMock;

    @Mock
    private EntityPreferenceRepository entityPreferenceRepositoryMock;

    @Mock
    private FacilityRepository facilityRepositoryMock;

    @Mock
    private MonitoringCenterRepository monitoringCenterRepositoryMock;

    @Mock
    private PatientRepository patientRepositoryMock;

    @Mock
    private PrescriptionRepository prescriptionRepositoryMock;

    @Mock
    private EcgStripService ecgStripServiceMock;

    @Configuration
    static class InjectionConfiguration {
    }

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = CustomECGDataException.class)
    public void eventDetails_calls_EventDetailsService_invalidEventID() {
        int expectedEcgStripId = 100;

        Mockito.when(ecgStripRepositoryMock.findById(expectedEcgStripId)).thenReturn(Optional.empty());

        eventDetailsService.getEventDetails(expectedEcgStripId);
    }

    @Test
    public void eventDetails_calls_EventDetailsService_validateEventDetails() {
        int expectedEcgStripId = 100;
        int expectedPrescriptionID = 200;
        int expectedCardiacEventID = 300;
        Integer expectedThresholdTypeID = 1;
        var expectedThresholdDescription = "Severe Bradycardia";
        Instant expectedReceivedDate = Instant.parse("2017-11-10T22:11:03.460446Z");

        ThresholdType expectedThresholdType = new ThresholdType(expectedThresholdTypeID, expectedThresholdDescription);
        CardiacEvent expectedCardiacEvent = new CardiacEvent(expectedCardiacEventID, /*expectedThresholdTypeID,*/ expectedThresholdType, "Fainted/Fell", "Resing");
        ECGStrip ecgStrip = new ECGStrip(expectedEcgStripId, expectedReceivedDate, expectedPrescriptionID , /*expectedCardiacEventID,*/ expectedCardiacEvent);

        Mockito.when(ecgStripRepositoryMock.findById(expectedEcgStripId)).thenReturn(Optional.of(ecgStrip));

        EventDetails data = eventDetailsService.getEventDetails(expectedEcgStripId);

        Assert.assertNotNull(data);
        Assert.assertEquals(expectedPrescriptionID, data.prescriptionID);
        Assert.assertEquals(expectedReceivedDate, data.receivedTime);
        Assert.assertEquals(Arrays.asList(expectedCardiacEvent.symptoms.split(",")), data.symptoms);
        Assert.assertEquals(expectedCardiacEvent.activities, data.activity);
        Assert.assertEquals(expectedThresholdType.description, data.thresholdType);
    }

    @Test
    public void eventDetails_calls_EventDetailsService_getEventsByPrescriptionIds() {
        int expectedEcgStripId = 100;
        int expectedPrescriptionID = 200;
        int expectedCardiacEventId = 300;
        int expectedDeviceID = 500;
        Integer expectedThresholdTypeID = 1;
        var expectedThresholdDescription = "Severe Bradycardia";
        int expectedThresholdValue = 3;
        int expectedLevel = 2;
        var expectedActivity = "Walking";
        EventStatus expectedEventStatus = EventStatus.Pending;
        Instant expectedReceivedDate = Instant.parse("2017-11-10T22:11:03.460446Z");
        Instant expectedStartDate = Instant.parse("2017-11-09T21:17:17.464446Z");
        List<String> expectedSymptoms = Arrays.asList("Fainted/Fell", "Dizziness");
        Instant expectedReviewedDate = Instant.parse("2021-12-17T22:11:03.460446Z");
        String expectedFindings = "expectedFindings";

        int expectedEcgStripId2 = 300;
        int expectedPrescriptionID2 = 99;
        int expectedCardiacEventId2 = 444;
        int expectedDeviceID2 = 555;
        Integer expectedThresholdTypeID2 = 14;
        var expectedThresholdDescription2 = "Ventricular Fibrillation";
        int expectedThresholdValue2 = 5;
        int expectedLevel2 = 1;
        var expectedActivity2 = "Biking";
        EventStatus expectedEventStatus2 = EventStatus.Processed;
        Instant expectedReceivedDate2 = Instant.parse("2021-10-17T21:21:17.777777Z");
        Instant expectedStartDate2 = Instant.parse("2021-10-16T21:17:17.464446Z");
        List<String> expectedSymptoms2 = Arrays.asList("Fatigue", "Short of Breath");
        Instant expectedReviewedDate2 = Instant.parse("2021-07-14T22:11:03.460446Z");
        int expectedConnectionTypeId = 1;
        String expectedFindings2 = "expectedFindings2";

        var expectedPrescriptionIDs = Arrays.asList(expectedPrescriptionID, expectedPrescriptionID2);

        var expectedEventDetails = new EventDetails(expectedEcgStripId, expectedReceivedDate, expectedThresholdDescription, expectedThresholdTypeID,
                expectedThresholdValue, expectedLevel, expectedSymptoms, expectedActivity, expectedPrescriptionID, expectedEventStatus, expectedCardiacEventId,
                expectedDeviceID, expectedReviewedDate, expectedConnectionTypeId, expectedFindings, expectedStartDate);
        var expectedEventDetails2 = new EventDetails(expectedEcgStripId2, expectedReceivedDate2, expectedThresholdDescription2, expectedThresholdTypeID2,
                expectedThresholdValue2, expectedLevel2, expectedSymptoms2, expectedActivity2, expectedPrescriptionID2, expectedEventStatus2, expectedCardiacEventId2,
                expectedDeviceID2, expectedReviewedDate2, expectedConnectionTypeId, expectedFindings2, expectedStartDate2);
        List<EventDetails> expectedEventDetailsList = new ArrayList<EventDetails>();
        expectedEventDetailsList.add(expectedEventDetails);
        expectedEventDetailsList.add(expectedEventDetails2);

        Mockito.when(ecgStripServiceMock.getEcgStripsByPrescriptionIds(expectedPrescriptionIDs)).thenReturn(expectedEventDetailsList);

        var actualEventDetailList = eventDetailsService.getEventsByPrescriptionId(expectedPrescriptionIDs);

        int index = 0;

        for (EventDetails actualEventDetails : actualEventDetailList) {
            Assert.assertEquals(actualEventDetails.activity, expectedEventDetailsList.get(index).activity);
            Assert.assertEquals(actualEventDetails.thresholdType, expectedEventDetailsList.get(index).thresholdType);
            Assert.assertEquals(actualEventDetails.prescriptionID, expectedEventDetailsList.get(index).prescriptionID);
            Assert.assertEquals(actualEventDetails.thresholdTypeId, expectedEventDetailsList.get(index).thresholdTypeId);
            Assert.assertEquals(actualEventDetails.eventId, expectedEventDetailsList.get(index).eventId);
            Assert.assertEquals(actualEventDetails.level, expectedEventDetailsList.get(index).level);
            Assert.assertEquals(actualEventDetails.thresholdValue, expectedEventDetailsList.get(index).thresholdValue);
            Assert.assertEquals(actualEventDetails.receivedTime, expectedEventDetailsList.get(index).receivedTime);
            Assert.assertEquals(actualEventDetails.symptoms, expectedEventDetailsList.get(index).symptoms);
            Assert.assertEquals(actualEventDetails.status, expectedEventDetailsList.get(index).status);
            Assert.assertEquals(actualEventDetails.legacyFindings, expectedEventDetailsList.get(index).legacyFindings);
            index++;
        };
    }

    @Test
    public void getParentStripForReportStrip_calls() {
        int expectedParentStripId = 100;
        int anyReportStripId = 200;

        ECGStrip ecgStrip = new ECGStrip();
        ecgStrip.ecgStripID = anyReportStripId;

        Mockito.when(ecgStripRepositoryMock.findById(anyReportStripId)).thenReturn(Optional.of(ecgStrip));
        Mockito.when(ecgStripRepositoryMock.isReportStrip(anyReportStripId)).thenReturn(true);
        Mockito.when(ecgStripRepositoryMock.getParentStripForReportStrip(anyReportStripId)).thenReturn(expectedParentStripId);

        int parentStripId = eventDetailsService.getParentStripForReportStrip(anyReportStripId);
        Assert.assertEquals(parentStripId, expectedParentStripId);
    }

    @Test(expected = CustomECGDataException.class)
    public void getParentStripForReportStrip_calls_InvalidReportStripId() {
        int expectedParentStripId = 100;
        int anyReportStripId = 200;
        int parentStripId = eventDetailsService.getParentStripForReportStrip(anyReportStripId);
        Assert.assertEquals(parentStripId, expectedParentStripId);
    }

    @Test
    public void getAssignedQueueForPrescriptionIds() {
        int expectedPrescriptionId = 10001;
        int expectedPatientId = 7777;
        int expectedFacilityId = 11;
        int expectedPhysicianId = 5454564;
        int expectedMedicalGroupId = 33333;
        var expectedPrescription = new Prescription();
        expectedPrescription.prescriptionId = expectedPrescriptionId;
        expectedPrescription.patientId = expectedPatientId;
        var expectedPatient = new Patient();
        expectedPatient.patientId = expectedPatientId;
        expectedPatient.facilityId = expectedFacilityId;
        expectedPatient.physicianId = expectedPhysicianId;
        var expectedPrescriptionIds = Arrays.asList(expectedPrescriptionId);
        var expectedPrescriptions = Arrays.asList(expectedPrescription);
        var expectedPatients = Arrays.asList(expectedPatient);
        var expectedPatientIds = Arrays.asList(expectedPatientId);
        var expectedFacility = new Facility();
        expectedFacility.facilityId = expectedFacilityId;
        expectedFacility.medicalGroupId = expectedMedicalGroupId;
        var expectedFacilityIds = Arrays.asList(expectedFacilityId);
        var expectedFacilities = Arrays.asList(expectedFacility);
        var expectedMonitoringCenterId1 = 87;
        var expectedMonitoringCenter1 = new MonitoringCenter(expectedMonitoringCenterId1, "MSA-1", 88, Instant.now(), 2);
        var expectedMonitoringCenterId2 = 67;
        var expectedMonitoringCenter2 = new MonitoringCenter(expectedMonitoringCenterId2, "MSA-2", 77, Instant.now(), 2);
        var expectedMonitoringCenterIds = Arrays.asList(expectedMonitoringCenterId1, expectedMonitoringCenterId2);
        var expectedMonitoringCenters = Arrays.asList(expectedMonitoringCenter1, expectedMonitoringCenter2);
        var expectedPreferencesPrescription = Arrays.asList(
          new EntityPreference(expectedPrescriptionId, EntityType.Prescription.getValue(),
                  Preference.PreferredMonitoringCenter.getValue(), String.valueOf(expectedMonitoringCenterId1), 6789, Instant.now(), null, 123)
        );
        var expectedPreferencesFacility = Arrays.asList(
                new EntityPreference(expectedFacilityId, EntityType.Location.getValue(),
                        Preference.PreferredMonitoringCenter.getValue(), String.valueOf(expectedMonitoringCenterId2), 6789, Instant.now(), null, 456)
        );

        Mockito.when(prescriptionRepositoryMock.findAllById(expectedPrescriptionIds)).thenReturn(expectedPrescriptions);
        Mockito.when(patientRepositoryMock.findAllById(expectedPatientIds)).thenReturn(expectedPatients);
        Mockito.when(facilityRepositoryMock.findAllById(expectedFacilityIds)).thenReturn(expectedFacilities);
        Mockito.when(entityPreferenceRepositoryMock.getEntityPreferenceForMultipleIds(expectedPrescriptionIds, EntityType.Prescription.getValue(), Preference.PreferredMonitoringCenter.getValue())).thenReturn(expectedPreferencesPrescription);
        Mockito.when(entityPreferenceRepositoryMock.getEntityPreferenceForMultipleIds(expectedFacilityIds, EntityType.Location.getValue(), Preference.PreferredMonitoringCenter.getValue())).thenReturn(expectedPreferencesFacility);
        Mockito.when(monitoringCenterRepositoryMock.findAllById(expectedMonitoringCenterIds)).thenReturn(expectedMonitoringCenters);

        var expectedAssignedEventQueue = new EventQueue(expectedMonitoringCenter2, expectedPrescriptionId);
        var actualAssignedEventQueue = eventDetailsService.getAssignedQueuesForPrescriptions(expectedPrescriptionIds).get(0);

        Assert.assertEquals(expectedAssignedEventQueue.queueName, actualAssignedEventQueue.queueName);
        Assert.assertEquals(expectedAssignedEventQueue.prescriptionId, actualAssignedEventQueue.prescriptionId);
    }
}
