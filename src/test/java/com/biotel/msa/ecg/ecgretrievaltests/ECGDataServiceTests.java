package com.biotel.msa.ecg.ecgretrievaltests;

import com.biotel.msa.ecg.retrieval.config.AppConfig;
import com.biotel.msa.ecg.retrieval.config.CustomECGDataException;
import com.biotel.msa.ecg.retrieval.model.dto.ECGData;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGRecord;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGStrip;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGStripECGRecord;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.Parameter;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.ECGRecordRepository;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.ECGStripECGRecordRepository;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.ECGStripRepository;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.ParameterRepository;
import com.biotel.msa.ecg.retrieval.service.ECGDataService;
import com.biotel.msa.ecg.retrieval.service.FileService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

@RunWith(JUnit4.class)
@ContextConfiguration
public class ECGDataServiceTests {

    @InjectMocks
    ECGDataService ecgDataService;

    @Mock
    private AppConfig appConfigMock;

    @Mock
    private ParameterRepository parameterRepositoryMock;

    @Mock
    private ECGStripECGRecordRepository ecgStripECGRecordRepositoryMock;

    @Mock
    private ECGStripRepository ecgStripRepositoryMock;

    @Mock
    private ECGRecordRepository ecgRecordRepositoryMock;

    @Mock
    private FileService fileServiceMock;

    @Configuration
    static class InjectionConfiguration {
    }

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = CustomECGDataException.class)
    public void getECGData_calls_ECGDataServiceService_invalidEventID() {

        int expectedEcgStripId = 100;
        ECGStrip ecgStrip = new ECGStrip();
        ecgStrip.ecgStripID = expectedEcgStripId;
        RelativePathAndEcgRecordIDTest relativePathAndEcgRecordIDTest = new RelativePathAndEcgRecordIDTest();

        Parameter parameter = new Parameter("Storage", "ecg", "\\TestFiles\\");
        Mockito.when(parameterRepositoryMock.findParameterByKeyNameAndParameterName("Storage", "ecg")).thenReturn(Optional.of(parameter));
        Mockito.when(ecgRecordRepositoryMock.getRelativePath(expectedEcgStripId)).thenReturn(relativePathAndEcgRecordIDTest);
        Mockito.when(ecgStripECGRecordRepositoryMock.findById(expectedEcgStripId)).thenReturn(Optional.empty());
        Mockito.when(ecgStripRepositoryMock.findById(expectedEcgStripId)).thenReturn(Optional.of(ecgStrip));
        try{
            Mockito.when(fileServiceMock.getEcgDataFromFiles(expectedEcgStripId, relativePathAndEcgRecordIDTest.getRelativePath()))
            .thenReturn(new ECGData(expectedEcgStripId, new byte[0], new byte[0], new byte[0], "C:\\Cardionet\\"));
        }
        catch(Exception ex)
        {}  
        ecgDataService.getECGData(expectedEcgStripId);     
    }

    @Test(expected = CustomECGDataException.class)
    public void getECGData_calls_ECGDataServiceService_invalidECGPath() {

        String path = "Invalid Path";
        int expectedEcgStripId = 100;

        ECGRecord ecgRecord = new ECGRecord(325946482, "");
        ECGStrip ecgStrip = new ECGStrip();
        ecgStrip.ecgStripID = 100;
        ECGStripECGRecord ecgStripECGRecord = new ECGStripECGRecord(ecgStrip, ecgRecord);
        RelativePathAndEcgRecordIDTest relativePathAndEcgRecordIDTest = new RelativePathAndEcgRecordIDTest();

        Parameter parameter = new Parameter("Storage", "ecg", "\\Invalid Path\\");
        Mockito.when(parameterRepositoryMock.findParameterByKeyNameAndParameterName("Storage", "ecg")).thenReturn(Optional.of(parameter));
        Mockito.when(ecgRecordRepositoryMock.getRelativePath(expectedEcgStripId)).thenReturn(relativePathAndEcgRecordIDTest);
        Mockito.when(ecgStripECGRecordRepositoryMock.findById(expectedEcgStripId)).thenReturn(Optional.of(ecgStripECGRecord));
        Mockito.when(ecgStripRepositoryMock.findById(expectedEcgStripId)).thenReturn(Optional.of(ecgStrip));

        ecgDataService.getECGData(expectedEcgStripId);
    }
}
