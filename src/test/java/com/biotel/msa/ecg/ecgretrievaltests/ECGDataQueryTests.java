package com.biotel.msa.ecg.ecgretrievaltests;

import com.biotel.msa.ecg.retrieval.api.graphql.query.ECGDataQuery;
import com.biotel.msa.ecg.retrieval.model.dto.ECGData;
import com.biotel.msa.ecg.retrieval.service.ECGDataService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;

@RunWith(JUnit4.class)
@TestPropertySource(locations = "classpath:application-local.properties")
public class ECGDataQueryTests {

    @InjectMocks
    ECGDataQuery ecgDataQuery;

    @Mock
    private ECGDataService ecgDataServiceMock;

    @Configuration
    static class InjectionConfiguration {
    }

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getECGData_calls_ECGDataService_getECGData() {
        int expectedEcgStripId = 10;
        ecgDataQuery.getECGData(expectedEcgStripId);
        Mockito.verify(ecgDataServiceMock).getECGData(expectedEcgStripId);
        Mockito.verifyNoMoreInteractions(ecgDataServiceMock);
    }

    @Test
    public void getECGData_returns_known_ECGData() {
        int anyEcgStripId = 10;
        ECGData expectedEcgData = getKnownECGData(anyEcgStripId);

        Mockito.when(ecgDataServiceMock.getECGData(Mockito.anyInt())).thenReturn(expectedEcgData);
        ECGData actualECGData = ecgDataQuery.getECGData(anyEcgStripId);

        Assert.assertSame(expectedEcgData, actualECGData);
        Assert.assertEquals(expectedEcgData.id, actualECGData.id);
        Assert.assertEquals(expectedEcgData.headerFile, actualECGData.headerFile);
        Assert.assertEquals(expectedEcgData.ecgDataFile, actualECGData.ecgDataFile);
        Assert.assertEquals(expectedEcgData.annotationFile, actualECGData.annotationFile);
    }

    private static ECGData getKnownECGData(int anyEcgStripId) {
        ECGData ecgData = new ECGData();

        ecgData.id = anyEcgStripId;
        ecgData.headerFile = new byte[] { 4, 5, 3, 4, 5, 6, 7, 7, 9, 10, 0, 1, 2 };
        ecgData.ecgDataFile =new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 1, 2};
        ecgData.annotationFile = new byte[] { 6, 5, 4, 3, 2, 1, 0, 1, 2, 3 };

        return ecgData;
    }

}
