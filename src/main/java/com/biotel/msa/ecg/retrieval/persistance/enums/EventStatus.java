package com.biotel.msa.ecg.retrieval.persistance.enums;

public enum EventStatus {
    Pending("Pending"),
    Processed("Processed");

    private final String eventStatus;

    EventStatus(String eventStatus) {
        this.eventStatus = eventStatus;
    }

    public static EventStatus valueOfStatus(String status) {
        for (EventStatus eventStatus : values()) {
            if (eventStatus.eventStatus.equals(status)) {
                return eventStatus;
            }
        }

        return null;
    }
}