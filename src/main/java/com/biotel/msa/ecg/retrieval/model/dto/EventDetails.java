package com.biotel.msa.ecg.retrieval.model.dto;

import com.biotel.msa.ecg.retrieval.persistance.enums.EventStatus;

import java.time.Instant;
import java.util.List;

public class EventDetails {
    public Instant receivedTime;
    public String thresholdType; /* Event trigger */
    public Integer thresholdTypeId;
    public Integer thresholdValue;
    public int level; /* Threshold Level */
    public int eventId;
    public EventStatus status;
    public List<String> symptoms;
    public String activity;
    public int prescriptionID;
    public int cardiacEventID;
    public int deviceID;
    public Instant reviewedDate;
    public int connectionTypeId;
    public String legacyFindings;
    public Instant startDate;

    public EventDetails() {

    }

    public EventDetails(
            int eventId, Instant receivedTime, String thresholdType, Integer thresholdTypeId, Integer thresholdValue, int level,
            List<String> symptoms, String activity, int prescriptionID, EventStatus status, int cardiacEventID, int deviceID,
            Instant reviewedDate, int connectionTypeId, String legacyFindings, Instant startDate
    ) {
        this.eventId = eventId;
        this.receivedTime = receivedTime;
        this.thresholdType = thresholdType;
        this.thresholdTypeId = thresholdTypeId;
        this.thresholdValue = thresholdValue;
        this.symptoms = symptoms;
        this.activity = activity;
        this.prescriptionID = prescriptionID;
        this.level = level;
        this.status = status;
        this.cardiacEventID = cardiacEventID;
        this.deviceID = deviceID;
        this.reviewedDate = reviewedDate;
        this.connectionTypeId = connectionTypeId;
        this.legacyFindings = legacyFindings;
        this.startDate = startDate;
    }
}
