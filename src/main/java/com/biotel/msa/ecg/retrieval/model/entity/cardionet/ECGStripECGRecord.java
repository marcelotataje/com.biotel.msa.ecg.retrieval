package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import org.springframework.data.domain.Persistable;

import javax.persistence.*;

/* Model class for 'ECGStripECGRecord' table
 * @author Ishwari Bhat
 * @Since 03/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */

@Table
@Entity
public class ECGStripECGRecord implements Persistable<Integer> {

    /*ECGStripID of an event  */
    @Id
    public int ecgStripID;
    @OneToOne(targetEntity = ECGRecord.class,cascade = CascadeType.ALL)
    @JoinColumn(name="ecgStripID", referencedColumnName = "ecgStripID", insertable = false, updatable = false)
    public ECGStrip ecgStrip;

    /*ECGRecord ID of an event */
//    public int ecgRecordID;
//    @Id
    @ManyToOne(targetEntity = ECGRecord.class,cascade = CascadeType.ALL)
    @JoinColumn(name="ecgRecordID", referencedColumnName = "ecgRecordID")
    public ECGRecord ecgRecord;

    public ECGStripECGRecord()
    {

    }

    public ECGStripECGRecord(ECGStrip ecgStrip, ECGRecord ecgRecord)
    {
        this.ecgStripID = ecgStrip.ecgStripID;
        this.ecgRecord = ecgRecord;
    }
    @Override
    public Integer getId() {
        return ecgStripID;
    }

    @Override
    public boolean isNew() {
        return true;
    }

}
