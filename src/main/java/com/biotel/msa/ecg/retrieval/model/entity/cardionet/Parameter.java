package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import javax.persistence.Entity;
import javax.persistence.Id;

/* Model class for 'Parameter' table
 * @author Ishwari Bhat
 * @Since 03/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */

@Entity
public class Parameter {

    public String keyName;
    @Id
    public String parameterName;
    public String characterValue;

    public Parameter()
    {

    }

    public Parameter(String keyName, String parameterName, String characterValue)
    {
        this.keyName = keyName;
        this.parameterName = parameterName;
        this.characterValue = characterValue;
    }
}
