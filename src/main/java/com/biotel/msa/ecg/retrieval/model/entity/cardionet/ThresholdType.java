package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import org.springframework.data.domain.Persistable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/* Model class for 'ThresholdType' table
 * @author Ishwari Bhat
 * @Since 06/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
@Entity
@Table
public class ThresholdType implements Persistable<Integer>
{
    @Id
    public int thresholdTypeID;
    public String description;

    ThresholdType()
    {

    }

    public ThresholdType(int thresholdTypeID, String description)
    {
        this.thresholdTypeID = thresholdTypeID;
        this.description = description;
    }

    @Override
    public Integer getId() {
        return thresholdTypeID;
    }

    @Override
    public boolean isNew() {
        return true;
    }
}
