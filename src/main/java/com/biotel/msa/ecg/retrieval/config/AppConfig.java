package com.biotel.msa.ecg.retrieval.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Value("${info.app.name}")
    public String appName;

    @Value("${info.app.version}")
    public String appVersion;
}
