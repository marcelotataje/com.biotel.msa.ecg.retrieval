package com.biotel.msa.ecg.retrieval.config;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomECGDataException extends RuntimeException implements GraphQLError {

    private Map<String, Object> extensions = new HashMap<>();
    String message;

    public CustomECGDataException(String message, String fieldname, Object arg) {
        this.message = message;
        extensions.put(fieldname, arg);
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return extensions;
    }

    @Override
    public ErrorType getErrorType()  {
        return null;
    }

    @Override 
    public String getMessage() {
        return message;
    }

}