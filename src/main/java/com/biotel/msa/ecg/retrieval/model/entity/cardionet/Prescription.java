package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import javax.persistence.Entity;
import javax.persistence.Id;

/* Model class for 'Prescription' table
 * @author Joel Nel
 * @Since 07/14/2021 <p/>
 * (c) 2021 BioTelemetry, Inc.
 */
@Entity
public class Prescription {
    @Id
    public int prescriptionId;
    public int patientId;

    public Prescription() {

    }
}
