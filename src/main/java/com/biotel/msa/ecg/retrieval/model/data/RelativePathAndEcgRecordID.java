package com.biotel.msa.ecg.retrieval.model.data;

public interface RelativePathAndEcgRecordID {   
    String getRelativePath();
    long getEcgRecordID();
}
