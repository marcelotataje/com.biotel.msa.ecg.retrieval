package com.biotel.msa.ecg.retrieval.service;

import com.biotel.msa.ecg.retrieval.exceptions.ECGRetrievalException;
import com.microsoft.sqlserver.jdbc.SQLServerDataTable;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import com.microsoft.sqlserver.jdbc.SQLServerPreparedStatement;
import com.zaxxer.hikari.pool.HikariProxyPreparedStatement;
import graphql.ErrorType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class FindByIdsService {
    @Autowired
    private DataSource dataSource;

    private static final Logger logger = LogManager.getLogger(FindByIdsService.class);

    public FindByIdsService() {

    }

    public ResultSet findByIds(String sql, List<Integer> ids, int fetchSize) {
        try {
            var sqlServerConnection = DataSourceUtils.getConnection(dataSource);
            var hikariProxyPreparedStatement = (HikariProxyPreparedStatement) sqlServerConnection.prepareStatement(sql);
            var sqlServerPreparedStatement = hikariProxyPreparedStatement.unwrap(SQLServerPreparedStatement.class);
            sqlServerPreparedStatement.setStructured(1, "dbo.FindByIds", getFindByIds(ids));
            ResultSet results = sqlServerPreparedStatement.executeQuery();
            results.setFetchSize(fetchSize);

            return results;
        } catch (SQLException sqlException) {
            throw new ECGRetrievalException("FindByIdsService SQLException Error Creating ECGStrips from ResultSet", ErrorType.DataFetchingException);
        }
    }

    private SQLServerDataTable getFindByIds(List<Integer> ids)  {
        var logIds = ids.stream().map(eventId -> String.valueOf(eventId)).collect(Collectors.joining(","));
        logger.info(String.format("Entered FindByIdsService getFindByIds for ids: %s.", logIds));

        try {
            SQLServerDataTable findByIds = new SQLServerDataTable();
            findByIds.addColumnMetadata("Id", Types.INTEGER);

            ids.parallelStream().forEach(id -> {
                try {
                    findByIds.addRow(id);
                } catch (SQLServerException sqlServerException) {
                    throw new ECGRetrievalException(String.format("SQLException Error adding to FindByIds table for id: %d", id), ErrorType.DataFetchingException);
                }
            });

            logger.info(String.format("Ended FindByIdsService getFindByIds for ids: %s.", logIds));

            return findByIds;
        } catch (SQLServerException sqlServerException) {
            throw new ECGRetrievalException("SQLException Error creating FindByIds table for Ids", ErrorType.DataFetchingException);
        }
    }
}
