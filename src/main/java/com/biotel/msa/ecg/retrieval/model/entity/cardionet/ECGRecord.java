package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import org.springframework.data.domain.Persistable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.time.LocalDateTime;

/* Model class for 'ECGRecord' table
 * @author Ishwari Bhat
 * @Since 03/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
@Table
@Entity
public class ECGRecord implements Persistable<Integer> {
    @Id
    /*ECG Record ID*/
    public int ecgRecordID;

    /*Relative path of the ECG Record*/
    public String relativePath;
    public int prescriptionID;
    public Instant startDate;
    public long durationInSeconds;
    public int sampleFrequency;
    public int channelSelection;
    public LocalDateTime createDate = LocalDateTime.now();
    public int createUserID;
    public int thresholdLevel;
    public boolean isActive = true;

    public ECGRecord()
    {

    }

    public ECGRecord(int ecgRecordID, String relativePath)
    {
        this.ecgRecordID = ecgRecordID;
        this.relativePath =relativePath;
    }

    public ECGRecord(int ecgRecordID, int prescriptionID, Instant startDate, long durationInSeconds, int createUserID, int thresholdLevel) {
        this.ecgRecordID = ecgRecordID;
        this.prescriptionID = prescriptionID;
        this.startDate = startDate;
        this.durationInSeconds = durationInSeconds;
        this.createUserID = createUserID;
        this.thresholdLevel = thresholdLevel;
    }
    @Override
    public Integer getId() {
        return ecgRecordID;
    }

    @Override
    public boolean isNew() {
        return true;
    }
}
