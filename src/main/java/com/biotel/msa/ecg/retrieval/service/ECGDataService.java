package com.biotel.msa.ecg.retrieval.service;

import com.biotel.msa.ecg.retrieval.config.AppConfig;
import com.biotel.msa.ecg.retrieval.config.CustomECGDataException;
import com.biotel.msa.ecg.retrieval.model.data.RelativePathAndEcgRecordID;
import com.biotel.msa.ecg.retrieval.model.dto.ECGData;
import com.biotel.msa.ecg.retrieval.model.dto.ECGRecordData;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGStrip;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGStripECGRecord;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.ECGRecordRepository;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.ECGStripECGRecordRepository;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.ECGStripRepository;
import io.micrometer.core.annotation.Timed;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.springframework.util.Assert.notNull;

/** This service has methods to get the ECG data from Parameter , ECGStrip, ECGStripECGRecord and ECGStripECGRecord tables
 * @author Ishwari Bhat
 * @{DATE} 03/06/2020<p/>
 * (c) 2020 BioTelemetry, Inc.
 */
@Service
public class ECGDataService{

    private static final Logger logger = LogManager.getLogger(ECGDataService.class);
    private ECGStripECGRecordRepository ecgStripECGRecordRepository;
    private ECGStripRepository ecgStripRepository;
    private AppConfig appConfig;
    private FileService fileService;
    private ECGRecordRepository ecgRecordRepository;
    
    public ECGDataService(
            final AppConfig appConfig,
            final ECGStripECGRecordRepository ecgStripECGRecordRepository,
            final ECGStripRepository ecgStripRepository,
            final ECGRecordRepository ecgRecordRepository,
            final FileService fileService
    ) {
        notNull(appConfig, "appConfig not injected.");
        notNull(ecgStripECGRecordRepository, "ecgStripECGRecordRepository not injected.");
        notNull(ecgStripRepository, "ecgStripRepository not injected.");
        notNull(ecgRecordRepository, "ecgRecordRepository not injected.");
        notNull(fileService, "fileService not injected.");

        this.appConfig = appConfig;
        this.ecgStripECGRecordRepository = ecgStripECGRecordRepository;
        this.ecgStripRepository = ecgStripRepository;
        this.ecgRecordRepository = ecgRecordRepository;
        this.fileService = fileService;
    }

    public String version() {
        return appConfig.appName + " " + appConfig.appVersion;
    }

    @Transactional(readOnly = true, isolation= Isolation.READ_UNCOMMITTED)
    public Optional<ECGStripECGRecord> getECGStripECGRecord(final int id) {
        return this.ecgStripECGRecordRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public String getECGRootDirectory()
    {
        return "\\ecgrecords\\";
    }

    /**
     * gets the ECGData from the database for a ECGStripID
     * @param ecgStripId
     * @return ECGData of the id.
     */
    @Timed(description = "ECGDataService - getECGData")
    public ECGData getECGData(int ecgStripId) 
    {
        try{
            long startTimefile = System.currentTimeMillis();
            String ecgFilePath = getECGFilePath(ecgStripId);       
            long endTimefile = System.currentTimeMillis();
            logger.info("getECGFilePath took " + (endTimefile - startTimefile) + " milliseconds");

            long startTime = System.currentTimeMillis();
            var ecgData = this.fileService.getEcgDataFromFiles(ecgStripId, ecgFilePath);
            long endTime = System.currentTimeMillis();
            logger.info("getEcgDataFromFiles took " + (endTime - startTime) + " milliseconds");
            if(!isDataFilesResultValid(ecgData, ecgStripId))
            {
                throw new CustomECGDataException("getECGData - Results from data files are not valid for ECGStripID: ", "ID", String.valueOf(ecgStripId));
            }

            return ecgData;
        }
        catch(Exception ex)
        {
            logger.error("Error on getting ecg data for ECGStripID: {}. Error: {}", ecgStripId, ex);
            throw new CustomECGDataException("getECGData - Error on getting ecg data for ECGStripID: "+ String.valueOf(ecgStripId), "error", ex);
        }      
    }

    @Timed(description = "ECGDataService - getECGRecordData")
    public ECGRecordData getECGRecordData(long ecgRecordId, List<String> requestedFields)
    {
        try{
            long startTimefile = System.currentTimeMillis();
            String ecgFilePath = getECGRecordFilePath(ecgRecordId);
            long endTimefile = System.currentTimeMillis();
            logger.info("getECGFilePath took " + (endTimefile - startTimefile) + " milliseconds");

            long startTime = System.currentTimeMillis();
            ECGRecordData ecgRecordData = this.fileService.getEcgRecordDataFromFiles(ecgRecordId, ecgFilePath, requestedFields);
            long endTime = System.currentTimeMillis();
            logger.info("getEcgDataFromFiles took " + (endTime - startTime) + " milliseconds");

            return ecgRecordData;
        }
        catch(Exception ex)
        {
            logger.error("Error on getting ecg data for ECGRecordID: {}. Error: {}", ecgRecordId, ex);
            throw new CustomECGDataException("getECGRecordData - Error on getting ecg data for ECGRecordID: "+ String.valueOf(ecgRecordId), "error", ex);
        }
    }

    private boolean isDataFilesResultValid(ECGData results, int ecgStripId)
    {
        if(results.headerFile == null)
        {
            logger.error("headerFile results is null for ECGStripID: {}.", ecgStripId);
            return false;
        }
        else if(results.ecgDataFile == null)
        {
            logger.error("ecgDataFile results is null for ECGStripID: {}.", ecgStripId);
            return false;
        }
        else if(results.annotationFile == null)
        {
            logger.error("annotationFile results is null for ECGStripID: {}.", ecgStripId);
            return false;
        }
        
        return true;
    }

    @Timed(description = "getECGFilePath")
    public String getECGFilePath(int id)
    {
        String rootDirectory = getECGRootDirectory();

        RelativePathAndEcgRecordID relativePathAndEcgRecordID = this.ecgRecordRepository.getRelativePath(id);
        if(!isRelativePathAndEcgRecordIDValid(id, relativePathAndEcgRecordID))
        {
            throw new CustomECGDataException("RelativePath and/or EcgRecordID are not valid.", "ID", String.valueOf(id));
        }

        String relativePath = relativePathAndEcgRecordID.getRelativePath();
        long ecgRecordID = relativePathAndEcgRecordID.getEcgRecordID();

        if(System.getProperty("os.name").toLowerCase().indexOf("nux") >=0)
       {
            return FilenameUtils.separatorsToUnix(rootDirectory+relativePath+ecgRecordID);
       }

       return rootDirectory+relativePath+ecgRecordID;
    }

    @Timed(description = "getECGFilePath")
    public String getECGRecordFilePath(long ecgRecordId)
    {
        String rootDirectory = getECGRootDirectory();
        String relativePath = this.ecgRecordRepository.getRelativePathECGRecord(ecgRecordId);

        if (relativePath == null || relativePath.trim().isEmpty())
        {
            logger.error("No RelativePath found for ECGRecordID: " + ecgRecordId);
            throw new CustomECGDataException("No RelativePath found for ECGRecordID", " ECGRecordID", String.valueOf(ecgRecordId));
        }

        if(System.getProperty("os.name").toLowerCase().indexOf("nux") >=0)
        {
            return FilenameUtils.separatorsToUnix(rootDirectory+relativePath+ecgRecordId);
        }

        return rootDirectory+relativePath+ecgRecordId;
    }

    private boolean isRelativePathAndEcgRecordIDValid(int id, RelativePathAndEcgRecordID results)
    {
        if(results == null)
        {
            logger.error("isRelativePathAndEcgRecordIDValid - Invalid ecgStripId: "+ id);
            return false;
        }
        else if(results.getEcgRecordID() <= 0)
        {
            logger.error("isRelativePathAndEcgRecordIDValid - No ECGRecord exists for the : "+ id);
            return false;
        }
        else if(results.getRelativePath() == null || results.getRelativePath().trim().isEmpty())
        {
            logger.error("isRelativePathAndEcgRecordIDValid - Invalid EcgRecordID: "+ results.getRelativePath());
            return false;
        }

        return true;
    }



    @Transactional(readOnly = true, isolation=Isolation.READ_UNCOMMITTED)   
    public Optional<ECGStrip> getECGStrip(final int id) {
        return this.ecgStripRepository.findById(id);
    }
}
