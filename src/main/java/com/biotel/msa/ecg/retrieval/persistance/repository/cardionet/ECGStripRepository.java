package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGStrip;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/* Repository for 'ECGStrip' table
 * @author Ishwari Bhat
 * @Since 03/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
public interface ECGStripRepository extends JpaRepository<ECGStrip, Integer> {
    @EntityGraph(value = "ecg-strip-graph")
    public Optional<ECGStrip> findById(int id);

    @Query(value= "SELECT PrescriptionID FROM ECGStrip (nolock) WHERE ECGStripID = ?1", nativeQuery=true)
    public int getPrescriptionIdForEcgStripId(int ecgStripId);

    @Query(value = "SELECT e1.ECGStripId FROM ECGStrip (nolock) e1, ECGStrip (nolock) e2 WHERE e1.IsForReport = 0 AND e1.CardiacEventId = e2.CardiacEventId AND e2. ECGStripId = ?1 AND e2.IsForReport = 1", nativeQuery=true)
    public Integer getParentStripForReportStrip(int ecgStripId);

    @Query(value = "SELECT IsForReport FROM ecgStrip(nolock) WHERE EcgStripId = ?1", nativeQuery=true)
    public boolean isReportStrip(int reportStripId);
}
