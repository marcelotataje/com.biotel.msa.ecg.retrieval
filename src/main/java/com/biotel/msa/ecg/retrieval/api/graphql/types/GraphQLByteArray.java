package com.biotel.msa.ecg.retrieval.api.graphql.types;

import graphql.language.StringValue;
import graphql.schema.*;
import org.springframework.context.annotation.Configuration;

import java.util.Base64;

/** Custom GraphQL type for the  Java ByteArray
 * @author Ishwari Bhat
 * @Since 03/09/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
@Configuration
public class GraphQLByteArray extends GraphQLScalarType {

    public GraphQLByteArray() {
        super("ByteArrayScalar", "Byte Array Scalar", new Coercing<byte[], Object>() {

            @Override
            public Object serialize(Object o) throws CoercingSerializeException {
                return  o;
            }

            @Override
            public byte[] parseValue(Object o) throws CoercingParseValueException {

               return Base64.getDecoder().decode(((String)o));
            }

            @Override
            public byte[] parseLiteral(Object o) throws CoercingParseLiteralException {

                return Base64.getDecoder().decode(((StringValue)o).getValue());
            }

        });
    }
}
