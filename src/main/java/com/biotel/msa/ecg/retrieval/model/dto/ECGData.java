package com.biotel.msa.ecg.retrieval.model.dto;

import javax.persistence.*;

/** Model class - 'ECGData' for the GraphQL response
 * @author Ishwari Bhat
 * @Since 03/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */

public class ECGData {
    @Id
    /*ECG Strip ID */
    public int id;

    /*ECG Header File in byte array*/
    public byte[] headerFile;

    /*ECG Data in byte array */
    public byte[] ecgDataFile;

    /*Annotations in byte array */
    public byte[] annotationFile;

    public String ecgFilePath;

    public ECGData() {
    }

    /**
     * Constructs the ECGData object.
     * @param id ECGStripID of the Event.
     * @param headerFile Byte array of ecg  header file.
     * @param ecgDataFile Byte array of ecg data file.
     * @param annotationFile byte array of ecg annotation file.
     */
    public ECGData(int id,  byte[] headerFile, byte[] ecgDataFile, byte[] annotationFile, String ecgFilePath)
    {
        this.id = id;
        this.headerFile = headerFile;
        this.ecgDataFile = ecgDataFile;
        this.annotationFile = annotationFile;
        this.ecgFilePath = ecgFilePath;
    }

}

