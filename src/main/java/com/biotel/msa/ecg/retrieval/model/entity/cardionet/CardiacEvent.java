package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import com.biotel.msa.ecg.retrieval.exceptions.ECGRetrievalException;
import com.biotel.msa.ecg.retrieval.util.InstantUtility;
import com.fasterxml.jackson.annotation.JsonAlias;
import graphql.ErrorType;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

/* Model class for 'CardiacEvent' table
 * @author Ishwari Bhat
 * @Since 06/03/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
@Table
@Entity
public class CardiacEvent implements Persistable<Integer> {
    @Id
    @Column(name = "CardiacEventID")
    public int cardiacEventID;
    public int thresholdTypeID;

    @ManyToOne(targetEntity = ThresholdType.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="thresholdTypeID", referencedColumnName ="thresholdTypeID", insertable = false, updatable = false)

    public ThresholdType thresholdType;
    public int thresholdLevel;
    @JsonAlias("CardiacEventStartDate")
    public Instant startDate;
    public Instant endDate;
    public int thresholdValue;
    public int count;
    public int monitorCardiacEventSeq;
    public int eventQuality;
    @Column(name = "ECGStripID")
    public int ecgStripId;
    public String symptoms;
    public String activities;

    public CardiacEvent(){}


    public CardiacEvent(int cardiacEventID, ThresholdType thresholdType, String symptoms, String activities) {
        this.cardiacEventID = cardiacEventID;
        this.thresholdType = thresholdType;
        this.symptoms =symptoms;
        this.activities = activities;
    }

    public CardiacEvent(int cardiacEventID, int thresholdTypeID, int thresholdLevel, Instant startDate, Instant endDate, int thresholdValue, int count, int monitorCardiacEventSeq, int eventQuality, int ecgStripId, String symptoms, String activities) {
        this.cardiacEventID = cardiacEventID;
        this.thresholdTypeID = thresholdTypeID;
        this.thresholdLevel = thresholdLevel;
        this.startDate = startDate;
        this.endDate = endDate;
        this.thresholdValue = thresholdValue;
        this.count = count;
        this.monitorCardiacEventSeq = monitorCardiacEventSeq;
        this.eventQuality = eventQuality;
        this.ecgStripId = ecgStripId;
        this.symptoms = symptoms;
        this.activities = activities;
    }

    public CardiacEvent(ResultSet resultSet) {
        try {
            this.cardiacEventID = resultSet.getInt("cardiacEventID");
            this.thresholdTypeID = resultSet.getInt("thresholdTypeID");
            this.thresholdLevel = resultSet.getInt("thresholdLevel");
            this.startDate = InstantUtility.instantFromDatabaseTimestamp(resultSet.getTimestamp("cardiacEventStartDate"));
            this.endDate = InstantUtility.instantFromDatabaseTimestamp(resultSet.getTimestamp("endDate"));
            this.thresholdValue = resultSet.getInt("thresholdValue");
            this.count = resultSet.getInt("count");
            this.monitorCardiacEventSeq = resultSet.getInt("monitorCardiacEventSeq");
            this.eventQuality = resultSet.getInt("eventQuality");
            this.ecgStripId = resultSet.getInt("ecgStripId");
            this.symptoms = resultSet.getString("symptoms");
            this.activities = resultSet.getString("activities");
        } catch (SQLException sqlException) {
            throw new ECGRetrievalException("SQLException Error Creating CardiacEvent from ResultSet", ErrorType.DataFetchingException);
        }
    }

    @Override
    public Integer getId() {
        return cardiacEventID;
    }

    @Override
    public boolean isNew() {
        return true;
    }
}