package com.biotel.msa.ecg.retrieval.persistance.enums;

import org.springframework.util.ObjectUtils;

public enum DeviceFamily {
    UNKNOWN(0, "UNKNOWN"),
    C2(1, "C2"),
    C3(2, "C3"),
    C5(3, "C5"),
    C6(4, "C6"),
    ER920W(5, "ER920W"),
    ECAT(6, "ECAT"),
    ER910AFIB(7, "ER910AFIB"),
    PER900(8, "PER900"),
    AFIB_DUAL(9, "AFIB_DUAL"),
    HEARTRAK_2(10, "HEARTRAK_2"),
    HEARTRAK_SMART(11, "HEARTRAK_SMART"),
    HEARTRAK_SMART_AF(12, "HEARTRAK_SMART_AF"),
    ROUND_LOOPER(13, "ROUND_LOOPER");

    /** Description of the DeviceFamily. */
    private final String description;

    /** Id of the DeviceFamily. */
    private final Integer id;

    /**
     * Constructs a DeviceFamily.

     * @param id the id of the DeviceFamily
     * @param description the description of the DeviceFamily
     */
    private DeviceFamily(int id, String description)
    {
        this.id = Integer.valueOf(id);
        this.description = description;
    }

    /**
     * Returns the description associated with this DeviceFamily.
     *
     * @return the description for this DeviceFamily
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Returns the id associated with this DeviceFamily.
     *
     * @return the id for this DeviceFamily
     */
    public Integer getId()
    {
        return id;
    }

    /**
     * Returns the id associated with this DeviceFamily as an int.
     *
     * @return the id for this DeviceFamily as an int
     */
    public int id()
    {
        return id.intValue();
    }

    /**
     * Converts the DeviceFamily to a string.
     *
     * @return the string identifier for the DeviceFamily.
     */
    public String toString()
    {
        return description;
    }

    /**
     * Gets the DeviceFamily associated with the given id.
     *
     * @param id the id of the desired DeviceFamily
     * @return the DeviceFamily associated with the given id
     * @throws IllegalArgumentException if the id does not represent any of the DeviceFamilys
     */
    public static DeviceFamily getDeviceFamily(int id)
    {
        return getDeviceFamily(Integer.valueOf(id));
    }

    /**
     * Gets the DeviceFamily associated with the given id.
     *
     * @param id the id of the desired DeviceFamily
     * @return the DeviceFamily associated with the given id
     * @throws IllegalArgumentException if the id does not represent any of the DeviceFamilys
     */
    public static DeviceFamily getDeviceFamily(Integer id)
    {
        if(ObjectUtils.isEmpty(id))
            throw new IllegalArgumentException("id cannot be null");
        for (DeviceFamily value : values())
        {
            if (value.getId().equals(id))
            {
                return value;
            }
        }

        throw new IllegalArgumentException("DeviceFamily with id " + id + " does not exist");
    }

    /**
     * Gets the DeviceFamily associated with the given description.
     *
     * @param description the description of the desired DeviceFamily
     * @return the DeviceFamily associated with the given description
     * @throws IllegalArgumentException if the description does not represent any of the DeviceFamilys
     */
    public static DeviceFamily getDeviceFamily(String description)
    {
        if(ObjectUtils.isEmpty(description))
            throw new IllegalArgumentException("description cannot be null");
        for (DeviceFamily value : values())
        {
            if (value.getDescription().equals(description))
            {
                return value;
            }
        }

        throw new IllegalArgumentException("DeviceFamily with description " + description + " does not exist");
    }
}
