package com.biotel.msa.ecg.retrieval.persistance.enums;

public enum Preference {
    Unknown(1),
    DailyReportDeliveryTime(2),
    CanEnrollOnline(3),
    DailyReportTemplate(4),
    UrgentReportTemplate(5),
    RequestedReportTemplate(6),
    SummaryReportTemplate(7),
    Language(8),
    PatientCategory(10),
    GenerateWeeklySummaryReport(11),
    WeeklySummaryTemplate(12),
    SendIfContainStrips(13),
    PreferredMonitoringCenter(14),
    AirStripEnabled(16),
    GenerateBaseLineReport(17),
    AutomateDailyReport(18),
    AutomateWeeklyReport(19),
    AutomateEOSReport(20),
    AutomateMCOTWeeklyReport(21),
    AutomateMCOTEOSReport(22),
    AutomateMCOTDailyReport(23),
    AFReportForOrderingPhysician(24),
    GeneratePIEReport(25),
    AutomateExtendedHolterDailyReport(26);

    private final Integer preferenceId;

    Preference(Integer preferenceId) {
        this.preferenceId = preferenceId;
    }

    public int getValue() {
        return preferenceId;
    }

    public static Preference valueOfId(Integer id) {
        for (Preference entityType : values()) {
            if (entityType.preferenceId.equals(id)) {
                return entityType;
            }
        }

        return null;
    }
}
