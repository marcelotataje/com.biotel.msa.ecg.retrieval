package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.EntityPreference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/* Repository for EntityPreference queries
 * @author Joel Nel
 * @Since 01/06/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
public interface EntityPreferenceRepository extends JpaRepository<EntityPreference, Integer> {
    @Query(value= "SELECT *" +
            " FROM EntityPreference (nolock)" +
            " WHERE EntityID = ?1" +
            " AND EntityTypeID = ?2 AND PreferenceID = ?3", nativeQuery=true)
    public EntityPreference getEntityPreference(int entityId, int entityTypeId, int preferenceId);

    @Query(value = "SELECT * FROM EntityPreference (nolock)" +
            " WHERE EntityID IN ?1" +
            " AND EntityTypeID = ?2 AND PreferenceID = ?3", nativeQuery = true)
    public List<EntityPreference> getEntityPreferenceForMultipleIds(List<Integer> entityIds, int entityTypeId, int preferenceId);
}
