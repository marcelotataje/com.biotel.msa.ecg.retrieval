package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.Facility;
import org.springframework.data.jpa.repository.JpaRepository;

/* Repository for 'Facility' table
 * @author Joel Nel
 * @Since 07/14/2021 <p/>
 * (c) 2021 BioTelemetry, Inc.
 */
public interface FacilityRepository extends JpaRepository<Facility, Integer> {
}
