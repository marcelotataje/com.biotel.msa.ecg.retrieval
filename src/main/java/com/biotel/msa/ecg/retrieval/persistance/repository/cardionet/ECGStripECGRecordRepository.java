package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGStripECGRecord;
import org.springframework.data.jpa.repository.JpaRepository;

/* Repository for 'ECGStripECGRecord' table
 * @author Ishwari Bhat
 * @Since 03/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
public interface ECGStripECGRecordRepository extends JpaRepository<ECGStripECGRecord, Integer> {
}
