package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.MonitoringCenter;
import org.springframework.data.jpa.repository.JpaRepository;

/* Repository for MonitoringCenter queries
 * @author Joel Nel
 * @Since 01/06/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
public interface MonitoringCenterRepository extends JpaRepository<MonitoringCenter, Integer> {

}
