package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import javax.persistence.Entity;
import javax.persistence.Id;

/* Model class for 'Patient' table
 * @author Joel Nel
 * @Since 07/14/2021 <p/>
 * (c) 2020=1 BioTelemetry, Inc.
 */
@Entity
public class Patient {
    @Id
    public int patientId;
    public int physicianId;
    public int facilityId;

    public Patient() {

    }
}