package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/* Repository for 'Parameter' table
 * @author Ishwari Bhat
 * @Since 03/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */

public interface ParameterRepository extends JpaRepository<Parameter, Integer> {

   public Optional<Parameter> findParameterByKeyNameAndParameterName(String keyName, String parameterName);

}
