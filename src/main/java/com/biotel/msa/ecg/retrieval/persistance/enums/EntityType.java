package com.biotel.msa.ecg.retrieval.persistance.enums;

public enum EntityType {
    Default(0),
    Patient(1),
    Physician(2),
    Location(3),
    User(4),
    Practice(5),
    Prescription(6),
    Report(7),
    UserGroup(8);

    private final Integer entityTypeId;

    EntityType(Integer entityTypeId) {
        this.entityTypeId = entityTypeId;
    }

    public int getValue() {
        return entityTypeId;
    }

    public static EntityType valueOfId(Integer id) {
        for (EntityType entityType : values()) {
            if (entityType.entityTypeId.equals(id)) {
                return entityType;
            }
        }

        return null;
    }
}
