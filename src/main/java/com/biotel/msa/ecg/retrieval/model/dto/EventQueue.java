package com.biotel.msa.ecg.retrieval.model.dto;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.MonitoringCenter;
import com.biotel.msa.ecg.retrieval.persistance.enums.EventStatus;

/** Model dto class - 'EventQueue' for the GraphQL response
 * @author Joel Nel
 * @Since 01/08/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
public class EventQueue {
    public String queueName;
    public Integer ecgStripId;
    public Integer prescriptionId;
    public EventStatus status;
    public Integer applicationVersionId;

    /**
     * Default constructor.
     */
    public EventQueue() {
    }

    public EventQueue(String queueName, int ecgStripId, EventStatus status, int applicationVersionId) {
        this.queueName = queueName;
        this.ecgStripId = ecgStripId;
        this.status = status;
        this.applicationVersionId = applicationVersionId;
    }

    public EventQueue(MonitoringCenter monitoringCenter, Integer prescriptionId) {
        this.queueName = monitoringCenter.description;
        this.prescriptionId = prescriptionId;
        this.applicationVersionId = monitoringCenter.applicationVersionId;
    }
}