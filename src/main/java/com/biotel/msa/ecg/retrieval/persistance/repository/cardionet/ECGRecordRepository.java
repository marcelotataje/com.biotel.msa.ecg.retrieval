package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.data.RelativePathAndEcgRecordID;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ECGRecordRepository extends JpaRepository<ECGRecord, Integer> {

    @Query(value= "SELECT RelativePath, eser.ECGRecordID " + 
    "FROM ECGStripECGRecord eser (nolock) " +
    "INNER JOIN ECGRecord er (nolock) on eser.ECGRecordID = er.ECGRecordID " + 
    "WHERE eser.ECGStripID = ?1 ", nativeQuery=true)
    public RelativePathAndEcgRecordID getRelativePath(int ecgStripId);

    @Query(value= "SELECT RelativePath " +
            "FROM ECGRecord  (nolock) " +
            "WHERE ECGRecordID = ?1 ", nativeQuery=true)
    public String getRelativePathECGRecord(long ecgRecordId);
}
