package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import javax.persistence.*;

/* Model class for 'Facility' table
 * @author Joel Nel
 * @Since 07/14/2021 <p/>
 * (c) 2021 BioTelemetry, Inc.
 */
@Entity
public class Facility {

    @Id
    public int facilityId;
    public Integer medicalGroupId;

    public Facility() {

    }
}
