package com.biotel.msa.ecg.retrieval.service;

import com.biotel.msa.ecg.retrieval.config.CustomECGDataException;
import com.biotel.msa.ecg.retrieval.model.dto.ECGRecordData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import io.micrometer.core.annotation.Timed;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import com.biotel.msa.ecg.retrieval.model.dto.ECGData;
import static org.springframework.util.Assert.notNull;

@Service
public class FileService {
    private static final Logger logger = LogManager.getLogger(FileService.class);

    private ExecutorService taskExecutor;

    public FileService(ExecutorService taskExecutor)
    {
        notNull(taskExecutor, "taskExecutor not injected.");

        this.taskExecutor = taskExecutor;
    }

    @Timed(description = "FileService - getEcgDataFromFiles")
    public ECGData getEcgDataFromFiles(int ecgStripId, String ecgFilePath) throws InterruptedException, ExecutionException, TimeoutException{

        Callable<byte[]> headerFileTask = () -> getHeaderFile(ecgFilePath+".hea"); 
        Callable<byte[]> ecgDataFileTask = () -> getECGDataFile(ecgFilePath+".dat");
        Callable<byte[]> annotationFileTask = () -> getAnnotationFile(ecgFilePath+".atr");

        Future<byte[]> headerFileResult = taskExecutor.submit(headerFileTask);
        Future<byte[]> ecgDataFileResult = taskExecutor.submit(ecgDataFileTask);
        Future<byte[]> annotationFileResult = taskExecutor.submit(annotationFileTask);

        byte[] headerFile = headerFileResult.get(30, TimeUnit.SECONDS);
        byte[] annotationFile = annotationFileResult.get(30, TimeUnit.SECONDS);
        byte[] ecgDataFile = ecgDataFileResult.get(30, TimeUnit.SECONDS);

        return new ECGData(ecgStripId, headerFile, ecgDataFile, annotationFile, ecgFilePath);
    }

    @Timed(description = "FileService - getEcgRecordDataFromFiles")
    public ECGRecordData getEcgRecordDataFromFiles(long ecgRecordID, String ecgFilePath, List<String> requestedFields) throws InterruptedException, ExecutionException, TimeoutException{
        byte[] headerFile = null;
        byte[] ecgDataFile = null;
        byte[] annotationFile = null;
        for (String field : requestedFields){
            if(field.equals("ecgHeaderFile")) {
                Callable<byte[]> headerFileTask = () -> getHeaderFile(ecgFilePath+".hea");
                Future<byte[]> headerFileResult = taskExecutor.submit(headerFileTask);
                headerFile = headerFileResult.get(30, TimeUnit.SECONDS);
                if(null == headerFile)
                    throw new CustomECGDataException("getECGRecordData - Results from "+ecgRecordID+".hea files are not valid for ECGRecordID: ", "ID", String.valueOf(ecgRecordID));
            }
            if(field.equals("ecgDataFile")) {
                Callable<byte[]> ecgDataFileTask = () -> getECGDataFile(ecgFilePath+".dat");
                Future<byte[]> ecgDataFileResult = taskExecutor.submit(ecgDataFileTask);
                ecgDataFile = ecgDataFileResult.get(30, TimeUnit.SECONDS);
                if (null == ecgDataFile)
                    throw new CustomECGDataException("getECGRecordData - Results from "+ecgRecordID+".dat files are not valid for ECGRecordID: ", "ID", String.valueOf(ecgRecordID));
            }
            if(field.equals("ecgAnnotationFile")) {
                Callable<byte[]> annotationFileTask = () -> getAnnotationFile(ecgFilePath+".atr");
                Future<byte[]> annotationFileResult = taskExecutor.submit(annotationFileTask);
                annotationFile = annotationFileResult.get(30, TimeUnit.SECONDS);
                if (null == annotationFile)
                    throw new CustomECGDataException("getECGRecordData - Results from "+ecgRecordID+".atr files are not valid for ECGRecordID: ", "ID", String.valueOf(ecgRecordID));
            }
        }
        return new ECGRecordData(ecgRecordID, ecgDataFile, headerFile, annotationFile);
    }

    @Timed(description = "FileService - Retrieving header file")
    public byte[] getHeaderFile(String filePath)
    {   
        long startTime = System.currentTimeMillis();
        var res = getBinaryDataForFile(filePath);
        long endTime = System.currentTimeMillis();
        logger.info("getHeaderFile took " + (endTime - startTime) + " milliseconds");

        return res;
    }

    @Timed(description = "FileService - Retrieving ECG data file")
    public byte[] getECGDataFile(String filePath)
    {
        long startTime = System.currentTimeMillis();
        var res = getBinaryDataForFile(filePath);
        long endTime = System.currentTimeMillis();
        logger.info("getECGDataFile took " + (endTime - startTime) + " milliseconds");

        return res;
    }

    @Timed(description = "FileService - Retrieving annotation file")
    public byte[] getAnnotationFile(String filePath)
    {
        long startTime = System.currentTimeMillis();
        var res = getBinaryDataForFile(filePath);
        long endTime = System.currentTimeMillis();
        logger.info("getAnnotationFile took " + (endTime - startTime) + " milliseconds");
        
        return res;
    }

    /*Reads the bytes from the file*/    
    public byte[] getBinaryDataForFile(String filePath)
    {
        var rawData = readSmallBinaryFile(filePath);
        if(null == rawData){
            logger.error("Unable to extract data file: "+filePath);
            return null;
        }       

        return rawData;
    }

    /*Reads the Binary File*/
    public byte[] readSmallBinaryFile(String fileName) {
        try {
            Path path = Paths.get(fileName);
            var res = Files.readAllBytes(path);
            return res;
        }catch(IOException e ){
            logger.error("Unable to read the bytes from file: "+fileName);
            return null;
        }
    }

}
