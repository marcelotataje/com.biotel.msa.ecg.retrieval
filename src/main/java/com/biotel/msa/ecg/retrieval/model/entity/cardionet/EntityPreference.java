package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

/** Model class - 'EntityPreference' for looking up event information
 * @author Joel Nel
 * @Since 01/06/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
@Entity
public class EntityPreference {
    @Id
    public int entityId;
    public int entityTypeId;
    public int preferenceId;
    public String value;
    public int updateUserId;
    public Instant updateDate;
    public Integer categoryId;
    public Integer entityPreferenceId;

    /**
     * Default constructor.
     */
    public EntityPreference() {
    }

    public EntityPreference(int entityId, int entityTypeId, int preferenceId, String value, int updateUserId, Instant updateDate, Integer categoryId, Integer entityPreferenceId) {
        this.entityId = entityId;
        this.entityTypeId = entityTypeId;
        this.preferenceId = preferenceId;
        this.value = value;
        this.updateUserId = updateUserId;
        this.updateDate = updateDate;
        this.categoryId = categoryId;
        this.entityPreferenceId = entityPreferenceId;
    }
}

