package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

/* Repository for 'Patient' table
 * @author Joel Nel
 * @Since 04/17/2021 <p/>
 * (c) 2021 BioTelemetry, Inc.
 */
public interface PatientRepository extends JpaRepository<Patient, Integer> {
}