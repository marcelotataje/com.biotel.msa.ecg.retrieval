package com.biotel.msa.ecg.retrieval.exceptions;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ECGRetrievalException extends RuntimeException implements GraphQLError {
    protected ErrorType errorType = null;
    protected Map<String, Object> extensions = new HashMap<>();
    protected String message;

    public ECGRetrievalException() {

    }

    public ECGRetrievalException(String message, String fieldname, String arg) {
        this.message = message;
        extensions.put(fieldname, arg);
    }

    public ECGRetrievalException(String message, ErrorType errorType) {
        this.message = message;
        this.errorType = errorType;
    }

    @Override
    public List<SourceLocation> getLocations() {
        return null;
    }

    @Override
    public Map<String, Object> getExtensions() {
        return extensions;
    }

    @Override
    public ErrorType getErrorType() {
        return errorType;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
