package com.biotel.msa.ecg.retrieval;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.context.annotation.Bean;

/** This service stores and retrieves the ECGData information to/from the database
 * @author Ishwari Bhat
 * @{DATE} 03/06/2020<p/>
 * (c) 2020 BioTelemetry, Inc.
 */
@ComponentScan("com.biotel.common")
@ComponentScan("com.biotel.msa.ecg.retrieval")
@SpringBootApplication
@EnableRabbit
@EnableTransactionManagement
public class ECGRetrievalServiceApplication {
    private static final Logger logger = LogManager.getLogger(ECGRetrievalServiceApplication.class);

    public static void main(String[] args) {

        logger.info("ECGRetrievalServiceApplication started");
        SpringApplication.run(ECGRetrievalServiceApplication.class, args);
    }

    @Bean (name = "taskExecutor")
    public ExecutorService taskExecutor() {
        logger.debug("Creating Async Task Executor");
        return Executors.newFixedThreadPool(10); 
    }

}
