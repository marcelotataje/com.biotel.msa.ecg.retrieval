package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.CardiacEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardiacEventRepository extends JpaRepository<CardiacEvent, Integer> {

}
