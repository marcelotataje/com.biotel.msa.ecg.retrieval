package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import com.biotel.msa.ecg.retrieval.exceptions.ECGRetrievalException;
import com.biotel.msa.ecg.retrieval.model.dto.EventDetails;
import com.biotel.msa.ecg.retrieval.persistance.enums.EventStatus;
import com.biotel.msa.ecg.retrieval.util.InstantUtility;
import graphql.ErrorType;
import org.springframework.data.domain.Persistable;

import javax.persistence.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/* Model class for 'ECGStrip' table
 * @author Ishwari Bhat
 * @Since 03/04/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */

@NamedEntityGraph(
        name = "ecg-strip-graph",
        attributeNodes = {
            @NamedAttributeNode(value = "cardiacEvent", subgraph = "cardiac-event-threshold-type")
        },
        subgraphs = {
            @NamedSubgraph(
                name = "cardiac-event-threshold-type",
                attributeNodes = {
                        @NamedAttributeNode("thresholdType")
                }
            )
        }
)
@Table
@Entity
public class ECGStrip implements Persistable<Integer> {

    /*ECGStripID of an event  */
    @Id
    public int ecgStripID;
    public Instant receivedDate;
    public int prescriptionID;
    public int deviceID;
    public LocalDateTime createDate = LocalDateTime.now();
    public int createUserID;
    public Instant startDate;
    public Instant reviewedDate;
    public long durationInSeconds;
    public boolean isForReport = false;
    public boolean isForEvent = false;
    public boolean isPause = false;
    public boolean isVt = false;
    public boolean isFullDisclosure = false;
    public int connectionTypeId = 0;
    public Boolean isDeleted;
    public String findings;

    @OneToOne(targetEntity = CardiacEvent.class,cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="cardiacEventID", referencedColumnName = "cardiacEventID")
    public CardiacEvent cardiacEvent;

    public ECGStrip() {

    }

    public ECGStrip(int ecgStripID, Instant receivedDate, int prescriptionID, CardiacEvent cardiacEvent) {
        this.ecgStripID = ecgStripID;
        this.receivedDate = receivedDate;
        this.prescriptionID = prescriptionID;
        this.cardiacEvent = cardiacEvent;
    }

    public ECGStrip(int ecgStripID,
                    int prescriptionID,
                    int deviceID,
                    CardiacEvent cardiacEvent,
                    int createUserID,
                    Instant startDate,
                    long durationInSeconds) {
        this.ecgStripID = ecgStripID;
        this.prescriptionID = prescriptionID;
        this.deviceID = deviceID;
        this.cardiacEvent = cardiacEvent;
        this.createUserID = createUserID;
        this.startDate = startDate;
        this.durationInSeconds = durationInSeconds;
    }

    public ECGStrip(ResultSet resultSet) {
        try {
            this.ecgStripID =  resultSet.getInt("ecgStripID");
            this.receivedDate = InstantUtility.instantFromDatabaseTimestamp(resultSet.getTimestamp("receivedDate"));
            this.prescriptionID = resultSet.getInt("prescriptionID");
            this.deviceID = resultSet.getInt("deviceID");
            this.createUserID = resultSet.getInt("createUserID");
            this.startDate = InstantUtility.instantFromDatabaseTimestamp(resultSet.getTimestamp("startDate"));
            this.reviewedDate = InstantUtility.instantFromDatabaseTimestamp(resultSet.getTimestamp("reviewedDate"));
            this.durationInSeconds = resultSet.getLong("durationInSeconds");
            this.isForReport = resultSet.getBoolean("isForReport");
            this.isDeleted = resultSet.getObject("isDeleted", Boolean.class);
            this.connectionTypeId = resultSet.getInt("connectionTypeID");

        } catch (SQLException sqlException) {
            throw new ECGRetrievalException("SQLException Error Creating ECGStrip from ResultSet", ErrorType.DataFetchingException);
        }
    }

    @Override
    public Integer getId() {
        return ecgStripID;
    }

    @Override
    public boolean isNew() {
        return true;
    }

    public EventDetails toEventDetails() {
        String activities = null;
        List<String> symptoms = null;
        String thresholdType = "";
        int thresholdLevel = 0;
        Integer thresholdTypeId = null;
        Integer thresholdValue = null;
        int cardiacEventID = 0;

        if (this.cardiacEvent != null && this.cardiacEvent.symptoms != null) {
            symptoms = Arrays.asList(this.cardiacEvent.symptoms.split(","));
            symptoms = symptoms.stream()
                    .map(String::trim)
                    .collect(Collectors.toList());
        }

        EventStatus status = this.reviewedDate != null ? EventStatus.Processed : EventStatus.Pending;

        if (this.cardiacEvent != null) {
            activities = this.cardiacEvent.activities;
            thresholdType = this.cardiacEvent.thresholdType != null ? this.cardiacEvent.thresholdType.description : "";
            thresholdLevel = this.cardiacEvent.thresholdLevel;
            thresholdTypeId = this.cardiacEvent.thresholdTypeID;
            thresholdValue = this.cardiacEvent.thresholdValue;
            cardiacEventID = this.cardiacEvent.cardiacEventID;
        }

        return new EventDetails(this.ecgStripID, this.receivedDate, thresholdType, thresholdTypeId, thresholdValue, thresholdLevel,
                symptoms, activities, this.prescriptionID, status, cardiacEventID, deviceID, reviewedDate, connectionTypeId, this.findings, this.startDate);
    }
}
