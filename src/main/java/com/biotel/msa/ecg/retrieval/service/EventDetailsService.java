package com.biotel.msa.ecg.retrieval.service;

import com.biotel.msa.ecg.retrieval.config.CustomECGDataException;
import com.biotel.msa.ecg.retrieval.exceptions.ECGRetrievalException;
import com.biotel.msa.ecg.retrieval.model.dto.EventDetails;
import com.biotel.msa.ecg.retrieval.model.dto.EventQueue;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.*;
import com.biotel.msa.ecg.retrieval.persistance.enums.EntityType;
import com.biotel.msa.ecg.retrieval.persistance.enums.EventStatus;
import com.biotel.msa.ecg.retrieval.persistance.enums.Preference;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.*;
import graphql.ErrorType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EventDetailsService {

    private static final Logger logger = LogManager.getLogger(ECGDataService.class);
  //  private CardiacEventRepository cardiacEventRepository;
    private ECGStripRepository ecgStripRepository;
    private EntityPreferenceRepository entityPreferenceRepository;
    private FacilityRepository facilityRepository;
    private MonitoringCenterRepository monitoringCenterRepository;
    private PatientRepository patientRepository;
    private PrescriptionRepository prescriptionRepository;
    private EcgStripService ecgStripService;

    public EventDetailsService(/*final CardiacEventRepository cardiacEventRepository,*/
                               final ECGStripRepository ecgStripRepository,
                               final EntityPreferenceRepository entityPreferenceRepository,
                               final FacilityRepository facilityRepository,
                               final MonitoringCenterRepository monitoringCenterRepository,
                               final PatientRepository patientRepository,
                               final PrescriptionRepository prescriptionRepository,

                               EcgStripService ecgStripService) {
       // this.cardiacEventRepository = cardiacEventRepository;
        this.ecgStripRepository = ecgStripRepository;
        this.entityPreferenceRepository = entityPreferenceRepository;
        this.facilityRepository = facilityRepository;
        this.monitoringCenterRepository = monitoringCenterRepository;
        this.patientRepository = patientRepository;
        this.prescriptionRepository = prescriptionRepository;
        this.ecgStripService = ecgStripService;
    }

    /**
     * gets the EventDetails from the database for a ECGStripID
     * @param id - ECGStripID
     * @return EventDetails data object
     */
    public EventDetails getEventDetails(int id) {
        var optStrip = getECGStrip(id);
        if (!optStrip.isPresent()) {
            logger.error(String.format("Invalid ID: %d", id));
            throw new CustomECGDataException("EventDetails - Invalid ID", "ID", String.valueOf(id));
        }

        ECGStrip ecgStrip = optStrip.get();

        return ecgStrip.toEventDetails();
    }

    /**
     * Gets EventDetails for a list of prescriptions by id
     * @param prescriptionIds A list of prescriptions to obtain EventDetails for
     * @return A List of EventDetail objects matching the prescriptionIds
     */
    public List<EventDetails> getEventsByPrescriptionId(List<Integer> prescriptionIds) {
        if (!prescriptionIds.isEmpty()) {
            return this.ecgStripService.getEcgStripsByPrescriptionIds(prescriptionIds);
        }
        else {
            return null;
        }
    }

    /**
     * Gets the assigned queue for the given Event.
     * @param eventId ID for the event
     * @return EventQueue object
     */
    public EventQueue getAssignedQueueForEvent(int eventId) {
        var ecgStrip = this.getECGStrip(eventId);

        if (ecgStrip.isEmpty()) {
            throw new ECGRetrievalException("ECG Strip ID is Invalid", ErrorType.DataFetchingException);
        }

        EventQueue eventQueue = null;
        var prescriptionId = this.getPrescriptionIdByEcgStripId(eventId);
        EventStatus status = ecgStrip.isPresent() && ecgStrip.get().receivedDate != null ? EventStatus.Processed : EventStatus.Pending;
        var eventQueues = getAssignedQueuesForPrescriptions(Arrays.asList(prescriptionId));

        if (eventQueues != null && eventQueues.size() > 0) {
            eventQueue = eventQueues.get(0);
            eventQueue.ecgStripId = eventId;
            eventQueue.status = status;
        }

        return eventQueue;
    }

    public List<EventQueue> getAssignedQueuesForPrescriptions(List<Integer> prescriptionIds) {
        List<EventQueue> eventQueues = new ArrayList<>();

        var prescriptions = this.getPrescriptionsByIds(prescriptionIds);
        var patientIds = prescriptions.stream().map(prescription -> prescription.patientId).collect(Collectors.toList());
        var patients = this.getPatientsByIds(patientIds);
        var physicianIds = patients.stream().map(patient -> patient.physicianId).collect(Collectors.toList());
        var facilityIds = patients.stream().map(patient -> patient.facilityId).collect(Collectors.toList());
        var facilities = this.getFacilitiesByIds(facilityIds);
        var medicalGroupIds = facilities.stream().map(facility -> facility.medicalGroupId).collect(Collectors.toList());

        var preferencesPrescriptions = this.getEntityPreferenceForIds(prescriptionIds, EntityType.Prescription.getValue(), Preference.PreferredMonitoringCenter.getValue());
        var preferencesPhysicians = this.getEntityPreferenceForIds(physicianIds, EntityType.Physician.getValue(), Preference.PreferredMonitoringCenter.getValue());
        var preferencesFacilities = this.getEntityPreferenceForIds(facilityIds, EntityType.Location.getValue(), Preference.PreferredMonitoringCenter.getValue());
        var preferencesPractices = this.getEntityPreferenceForIds(medicalGroupIds, EntityType.Practice.getValue(), Preference.PreferredMonitoringCenter.getValue());
        List<Integer> monitoringCenterIds = new ArrayList<Integer>();
        monitoringCenterIds.addAll(preferencesPrescriptions.stream().map(monitoringCenterPreference -> Integer.valueOf(monitoringCenterPreference.value)).distinct().collect(Collectors.toList()));
        monitoringCenterIds.addAll(preferencesPhysicians.stream().map(monitoringCenterPreference -> Integer.valueOf(monitoringCenterPreference.value)).distinct().collect(Collectors.toList()));
        monitoringCenterIds.addAll(preferencesFacilities.stream().map(monitoringCenterPreference -> Integer.valueOf(monitoringCenterPreference.value)).distinct().collect(Collectors.toList()));
        monitoringCenterIds.addAll(preferencesPractices.stream().map(monitoringCenterPreference -> Integer.valueOf(monitoringCenterPreference.value)).distinct().collect(Collectors.toList()));
        monitoringCenterIds = monitoringCenterIds.stream().distinct().collect(Collectors.toList());
        List<MonitoringCenter> monitoringCenters = this.monitoringCenterRepository.findAllById(monitoringCenterIds);

        prescriptions.forEach(prescription ->  {
            var patient = getPrescriptionPatient(prescription, patients);
            var facility = getPatientFacility(patient, facilities);
            EntityPreference entityPreference = null;

            var preferencePrescription = preferencesPrescriptions.stream().filter(preference -> preference.entityId == prescription.prescriptionId).findFirst();
            if (preferencePrescription.isPresent()) {
                entityPreference = preferencePrescription.get();
            }

            var preferencePhysician = preferencesPhysicians.stream().filter(preference -> preference.entityId == patient.physicianId).findFirst();
            if (preferencePhysician.isPresent()) {
                entityPreference = preferencePhysician.get();
            }

            var preferenceFacility = preferencesFacilities.stream().filter(preference -> preference.entityId == patient.facilityId).findFirst();
            if (preferenceFacility.isPresent()) {
                entityPreference = preferenceFacility.get();
            }

            var preferencePractice = preferencesPractices.stream().filter(preference -> preference.entityId == facility.medicalGroupId).findFirst();
            if (preferencePractice.isPresent()) {
                entityPreference = preferencePractice.get();
            }

            if (entityPreference != null) {
                MonitoringCenter monitoringCenter = getMonitoringCenterById(monitoringCenters, Integer.parseInt(entityPreference.value));
                eventQueues.add(new EventQueue(monitoringCenter, prescription.prescriptionId));
            }
        });

        return eventQueues;
    }

    @Transactional(readOnly = true, isolation=Isolation.READ_UNCOMMITTED)
    public Optional<ECGStrip> getECGStrip(final int id) {
        return this.ecgStripRepository.findById(id);
    }

    @Transactional(readOnly = true, isolation=Isolation.READ_UNCOMMITTED)
    public boolean isReportStrip(final int id) {
        return this.ecgStripRepository.isReportStrip(id);
    }

    @Transactional
    public int getPrescriptionIdByEcgStripId(int ecgStripId) {
        return ecgStripRepository.getPrescriptionIdForEcgStripId(ecgStripId);
    }

    @Transactional
    public EntityPreference getEntityPreference(int entityId, int entityTypeId, int preferenceId) {
        return entityPreferenceRepository.getEntityPreference(entityId, entityTypeId, preferenceId);
    }

    @Transactional
    public List<EntityPreference> getEntityPreferenceForIds(List<Integer> entityIds, int entityTypeId, int preferenceId) {
        return entityPreferenceRepository.getEntityPreferenceForMultipleIds(entityIds, entityTypeId, preferenceId);
    }

    @Transactional
    public Integer getParentStripForReportStrip(int reportStripId) {
        if (!getECGStrip(reportStripId).isPresent()) {
            logger.error("Invalid ecgStripId: "+reportStripId);
            throw new CustomECGDataException("ParentStripForReportStrip - Invalid ECGStripId", "reportStripId", String.valueOf(reportStripId));
        }

        if (!isReportStrip(reportStripId)) {
            logger.error("Invalid report StripId: "+reportStripId);
            throw new CustomECGDataException("ParentStripForReportStrip - Invalid ReportStripId", "reportStripId", String.valueOf(reportStripId));
        }

       Integer parentStripId =  ecgStripRepository.getParentStripForReportStrip(reportStripId);

        if (parentStripId == null) {
            logger.error("No Parent Strip found for ReportStripID " + reportStripId + ".");
            throw new CustomECGDataException("No Parent Strip found for ReportStrip ID", "ECGStripID", String.valueOf(reportStripId));
        }

        return parentStripId;
    }

    @Transactional
    private List<Facility> getFacilitiesByIds(List<Integer> facilityIds) {
        return facilityRepository.findAllById(facilityIds);
    }

    @Transactional
    private List<Prescription> getPrescriptionsByIds(List<Integer> prescriptionIds) {
        return prescriptionRepository.findAllById(prescriptionIds);
    }

    @Transactional
    private List<Patient> getPatientsByIds(List<Integer> patientIds) {
        return patientRepository.findAllById(patientIds);
    }

    private MonitoringCenter getMonitoringCenterById(List<MonitoringCenter> monitoringCenters, int id) {
        var result = monitoringCenters.stream().filter(monitoringCenter -> monitoringCenter.monitoringCenterId == id).findFirst();
        return result.orElse(null);
    }

    private Facility getPatientFacility(Patient patient, List<Facility> facilities) {
        if (facilities == null) {
            return null;
        }

        var facility = facilities.stream().filter(f -> f.facilityId == patient.facilityId).findFirst();
        return facility.isPresent() ? facility.get() : null;
    }

    private Patient getPrescriptionPatient(Prescription prescription, List<Patient> patients) {
        if (patients == null) {
            return null;
        }

        var patient = patients.stream().filter(p -> p.patientId == prescription.patientId).findFirst();
        return patient.isPresent() ? patient.get() : null;
    }
}
