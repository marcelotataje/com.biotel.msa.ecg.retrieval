package com.biotel.msa.ecg.retrieval.api.graphql.query;

import com.biotel.msa.ecg.retrieval.model.dto.ECGData;
import com.biotel.msa.ecg.retrieval.model.dto.ECGRecordData;
import com.biotel.msa.ecg.retrieval.model.dto.EventDetails;
import com.biotel.msa.ecg.retrieval.model.dto.EventQueue;
import com.biotel.msa.ecg.retrieval.service.ECGDataService;
import com.biotel.msa.ecg.retrieval.service.EventDetailsService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import graphql.schema.DataFetchingEnvironment;
import io.micrometer.core.annotation.Timed;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author Ishwari Bhat
 * @Since 03/09/2020 <p/>
 * (c) 2020 BioTelemetry, Inc.
 */
@Component
public class ECGDataQuery implements GraphQLQueryResolver {

    private static final Logger logger = LogManager.getLogger(ECGDataQuery.class);
    private ECGDataService ecgDataService;
    private EventDetailsService eventDetailsService;

    /**
     * Constructs the ECGDataQuery class.
     * @param ecgDataService Gets ecg data to return as part of query.
     * @param eventDetailsService Gets event details to return as part of query.
     */
    public ECGDataQuery(
            ECGDataService ecgDataService,
            EventDetailsService eventDetailsService)
    {
        this.ecgDataService = ecgDataService;
        this.eventDetailsService = eventDetailsService;
    }

    /**
     * Retrieves the application version
     * @return String containing application name and version
     */
    @Timed(description = "ECGRetrieval - Retrieving App Version")
    public String version() {
        logger.info("Entered ECGRetrieval-version");
        var ret = this.ecgDataService.version();
        logger.info("Ended ECGRetrieval-version");
        return ret;
    }

    /**
     * Returns the ECGData for specified ecgStripId.
     * @param id ID of the ECG data to return.
     * @return ECGData object representing ECG data.
     * @implNote Called by the waveform service to retrieve the ecg data.
     */
    @Timed(description = "ECGRetrieval - Retrieving ECGData")
    public ECGData getECGData(final int id) {
        long startTime = System.currentTimeMillis();
        logger.info("Entered ECGDataQuery for ECGData : EventID " + id);
        ECGData ecgData = this.ecgDataService.getECGData(id);
        logger.info("Ended ECGDataQuery for ECGData : EventID " + id);
        long endTime = System.currentTimeMillis();
        logger.info("getECGData api call took " + (endTime - startTime) + " milliseconds");

        return ecgData;
    }

    /**
     * Returns the ECGRecordData for specified ecgRecordId.
     * @param id ID of the ECG data to return.
     * @return ECGRecordData object representing ECG data.
     * @implNote Called by the cardiologs-ecg-uploader service to retrieve the ecg data.
     */
    @Timed(description = "ECGRetrieval - Retrieving ECGRecordData")
    public ECGRecordData getECGRecordData(final long id, DataFetchingEnvironment dataFetchingEnvironment) {
        List requestedFields = Arrays.asList(dataFetchingEnvironment.getSelectionSet()
                .get()
                .keySet().toArray());
        long startTime = System.currentTimeMillis();
        logger.info("Entered ECGRecordDataQuery for ECGRecordData : EventID " + id);
        ECGRecordData ecgData = this.ecgDataService.getECGRecordData(id, requestedFields);
        logger.info("Ended ECGRecordDataQuery for ECGRecordData : EventID " + id);
        long endTime = System.currentTimeMillis();
        logger.info("getECGData api call took " + (endTime - startTime) + " milliseconds");

        return ecgData;
    }

    /**
     * Returns the ECGData for specified ecgStripId.
     * @param id ID of the ECG data to return.
     * @return ECGData object representing ECG data.
     * @implNote Called by the waveform service to retrieve the ecg data.
     */
    @Timed(description = "ECGRetrieval - Retrieving EventDetails")
    public EventDetails getEventDetails(final int id) {
        logger.info("Entered ECGDataQuery for EventDetails : EventID " + id);
        EventDetails eventDetails = this.eventDetailsService.getEventDetails(id);
        logger.info("Ended ECGDataQuery for EventDetails : EventID " + id);

        return eventDetails;
    }

    /**
     * Returns the ECGData for specified prescription IDs.
     * @param prescriptionIds List of prescription IDs for the service to match to ECG Details
     * @return List of ECGData objects representing ECG data.
     * @implNote Called by the search service to retrieve a list fo ECG data based on prescription ID.
     */
    @Timed(description = "ECGRetrieval - Retrieving EventDetails For PrescriptionIDs")
    public List<EventDetails> getEventsByPrescriptionIds(final List<Integer> prescriptionIds) {
        logger.info("Entered ECGDataQuery for GetEventsByPrescriptionIds");
        List<EventDetails> eventDetails = this.eventDetailsService.getEventsByPrescriptionId(prescriptionIds);
        logger.info("Ended ECGDataQuery for GetEventsByPrescriptionIds");

        return eventDetails;
    }

    @Timed(description = "ECGRetrieval - Retrieving AssignedQueue for EventDetail")
    public EventQueue assignedQueueForEvent(final Integer eventId) {
        logger.info("Entered ECGDataQuery for assignedQueueForEvent");
        EventQueue assignedQueue = this.eventDetailsService.getAssignedQueueForEvent(eventId);
        logger.info("Entered ECGDataQuery for assignedQueueForEvent");

        return assignedQueue;
    }

    @Timed(description = "ECGRetrieval - Retrieving Assigned Queue For Prescriptions")
    public List<EventQueue> getAssignedQueuesForPrescriptions(final List<Integer> prescriptionIds) {
        logger.info("Entered ECGDataQuery for getAssignedQueuesForPrescriptions");
        List<EventQueue> eventQueues = this.eventDetailsService.getAssignedQueuesForPrescriptions(prescriptionIds);
        logger.info("Ended ECGDataQuery for getAssignedQueuesForPrescriptions");

        return eventQueues;
    }

    @Timed(description = "ECGRetrieval - Retrieving ParentStripIDForReportStripId")
    public Integer parentStripForReportStrip(final int reportStripId) {
        logger.info("Entered ECGDataQuery for getParentStripForReportStrip");
        Integer parentStripId = this.eventDetailsService.getParentStripForReportStrip(reportStripId);
        logger.info("Ended ECGDataQuery for getParentStripForReportStrip");

        return parentStripId;
    }
}
