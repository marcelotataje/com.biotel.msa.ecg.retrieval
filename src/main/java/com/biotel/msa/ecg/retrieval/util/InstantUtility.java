package com.biotel.msa.ecg.retrieval.util;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * The InstantUtility class contains static methods that execute frequently
 * performed tasks related to DateTime values retrieved from the database.
 *
 * @author  Joel Nel
 * @version Version information maintained by source control system
 */
public class InstantUtility {

    public static Instant instantFromDatabaseTimestamp(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }

        return ZonedDateTime.of(timestamp.toLocalDateTime(), ZoneId.of("UTC")).toInstant();
    }
}
