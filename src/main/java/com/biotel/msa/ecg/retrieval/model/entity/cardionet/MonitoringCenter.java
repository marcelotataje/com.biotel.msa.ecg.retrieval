package com.biotel.msa.ecg.retrieval.model.entity.cardionet;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;

@Entity
public class MonitoringCenter {
    @Id
    public int monitoringCenterId;
    public String description;
    public int loadPercentage;
    public Instant updateDate;
    public int applicationVersionId;

    /**
     * Default constructor.
     */
    public MonitoringCenter() {
    }

    public MonitoringCenter(int monitoringCenterId, String description, int loadPercentage, Instant updateDate, int applicationVersionId) {
        this.monitoringCenterId = monitoringCenterId;
        this.description = description;
        this.loadPercentage = loadPercentage;
        this.updateDate = updateDate;
        this.applicationVersionId = applicationVersionId;
    }
}
