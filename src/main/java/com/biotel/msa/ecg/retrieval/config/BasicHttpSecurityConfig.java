package com.biotel.msa.ecg.retrieval.config;

import com.google.common.net.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.util.matcher.RequestMatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * Configures basic authentication. The order is set to 2, so that this filter occurs before the resource server
 * filter. As well, this configuration is limited to requests that have a Basic authentication header. All other
 * requests will go through the OAuth filter.
 */
@Configuration
@Order(2)
public class BasicHttpSecurityConfig extends WebSecurityConfigurerAdapter
{
    @Value("${management.server.port}")
    private int managementPort;

    @Value("${authorize.user}")
    private String authorizeUser;

    @Value("${authorize.password}")
    private String authorizePassword;

    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http
            .requestMatcher(request -> {
                var authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
                return (authHeader != null && authHeader.startsWith("Basic"));
            })
            .csrf().disable()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .authorizeRequests()
            .requestMatchers(checkPort(managementPort)).permitAll()
            .anyRequest().authenticated()
            .and().httpBasic();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.inMemoryAuthentication()
                .withUser(authorizeUser)
                .password("{noop}" + authorizePassword)
                .roles("USER");
    }

    /**
     * This method verifies whether a request port is equal to the provided method parameter
     *
     * @param port Port that needs to be checked with the incoming request port
     * @return Returns a request matcher object with port comparison
     */
    private RequestMatcher checkPort(final int port) {
        return (HttpServletRequest request) -> port == request.getLocalPort();
    }

}
