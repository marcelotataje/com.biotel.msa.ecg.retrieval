package com.biotel.msa.ecg.retrieval.persistance.repository.cardionet;

import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ThresholdType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ThresholdTypeRepository extends JpaRepository<ThresholdType, Integer> {
}
