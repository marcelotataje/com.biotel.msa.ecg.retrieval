package com.biotel.msa.ecg.retrieval.service;

import com.biotel.msa.ecg.retrieval.exceptions.ECGRetrievalException;
import com.biotel.msa.ecg.retrieval.model.dto.EventDetails;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.CardiacEvent;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ECGStrip;
import com.biotel.msa.ecg.retrieval.model.entity.cardionet.ThresholdType;
import com.biotel.msa.ecg.retrieval.persistance.repository.cardionet.ThresholdTypeRepository;
import graphql.ErrorType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EcgStripService {

    private static final Logger logger = LogManager.getLogger(EcgStripService.class);
    private FindByIdsService findByIdsService;
    private ThresholdTypeRepository thresholdTypeRepository;

    public EcgStripService(FindByIdsService findByIdsService, ThresholdTypeRepository thresholdTypeRepository) {
        this.findByIdsService = findByIdsService;
        this.thresholdTypeRepository = thresholdTypeRepository;
    }

    @Transactional
    public List<EventDetails> getEcgStripsByPrescriptionIds(List<Integer> prescriptionIds) {
        var logPrescriptionIds = prescriptionIds.stream().map(prescriptionId -> String.valueOf(prescriptionId)).collect(Collectors.joining(","));
        logger.info(String.format("Entered EcgStripService getEcgStripsByPrescriptionIds for PrescriptionIds: %s.", logPrescriptionIds));

        var start = Instant.now();
        List<EventDetails> eventDetailsList = new ArrayList<EventDetails>();

        var thresholdTypes = this.thresholdTypeRepository.findAll();

        try {
            var results = findByIdsService.findByIds("EXEC ECGStripCardiacEventFindByPrescriptionIDs ?", prescriptionIds, 1000);
            while (results.next()) {
                var ecgStrip = new ECGStrip(results);
                ecgStrip.cardiacEvent = new CardiacEvent(results);
                setCardiacEventThresholdType(ecgStrip, thresholdTypes);
                eventDetailsList.add(ecgStrip.toEventDetails());
            }
        }
        catch (SQLException sqlException) {
            throw new ECGRetrievalException("SQLException Error Creating ECGStrips from ResultSet", ErrorType.DataFetchingException);
        }

        logger.info(String.format("Ended EcgStripService getEcgStripsByPrescriptionIds for PrescriptionIds: %s.", logPrescriptionIds));
        logger.info(String.format("EcgStripService getEcgStripsByPrescriptionIds runtime: %d ms", Duration.between(start, Instant.now()).toMillis()));

        return eventDetailsList;
    }

    private void setCardiacEventThresholdType(ECGStrip strip, List<ThresholdType> thresholdTypes) {
        if (strip.cardiacEvent != null) {
            var type = thresholdTypes.stream().filter(thresholdType -> thresholdType.thresholdTypeID == strip.cardiacEvent.thresholdTypeID).findFirst();
            strip.cardiacEvent.thresholdType = type.orElse(null);
        }
    }
}
