package com.biotel.msa.ecg.retrieval.model.dto;

import javax.persistence.Id;

public class ECGRecordData
{
    @Id
    public long ecgRecordId;
    public byte[] ecgDataFile;
    public byte[] ecgHeaderFile;
    public byte[] ecgAnnotationFile;

    public ECGRecordData() {
    }

    public ECGRecordData(long ecgRecordId, byte[] ecgDataFile, byte[] ecgHeaderFile, byte[] ecgAnnotationFile)
    {
        this.ecgRecordId = ecgRecordId;
        this.ecgDataFile = ecgDataFile;
        this.ecgHeaderFile = ecgHeaderFile;
        this.ecgAnnotationFile = ecgAnnotationFile;
    }
}
